# Backbone App
## Mini Challenge 3

| Branch Name      | Description | Named Format |
| ---------------- | ----------- | ------------ |
| master           | Hold stable releases from develop | 
| develop          | Hold development state app then will be merged to master branch |
| story-branch   | Hold development state of a story then will be merged to develop branch | story/{story-number}/story-name
| task-branch      | Hold part of the story-branch | task/{story-number}{task-number}/task-name

> Example for story-branch => story/01/products-features (max. 5 word)

> Example for task-branch from story-branch example above => task/0101/add-form-create-new-products (max. 5 word)

### So the repository structure will be
* master branch
  * develop branch
    * story branch
      * task branch
