//
//  PortoflioApp.swift
//  Portoflio
//
//  Created by Henry David Lie on 27/09/21.
//

import SwiftUI

@main
struct PortoflioApp: App {
    let persistenceController = PersistenceController.shared

    var body: some Scene {
        WindowGroup {
            ContentView()
                .environment(\.managedObjectContext, persistenceController.container.viewContext)
        }
    }
}
